import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore';

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyB4RDGG32ZTjdM65WR0yvMUUQubjXAwTXs",
  authDomain: "house-marketplace-app-163bb.firebaseapp.com",
  projectId: "house-marketplace-app-163bb",
  storageBucket: "house-marketplace-app-163bb.appspot.com",
  messagingSenderId: "776241264438",
  appId: "1:776241264438:web:83f5cf66fff86c73d9e597"
};

// Initialize Firebase
initializeApp(firebaseConfig);
export const db = getFirestore();
