# Deploy to Heroku

Have to install [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli).

Then login to Heroku from command line:
```shell
heroku login
```

Create application in Heroku:
```shell
heroku create mysupportdesk
```
**NOTE:** name must be unique.

Open application in Heroku admin console and go to Settings and click 'Reveal Config Vars' button.
Add following environment variables:

| Variable Name | Variable Value            |
|---------------|---------------------------|
| NODE_ENV      | production                |
| MONGO_URI     | \<mongo DB product URI\>  |
| JWT_SECRET    | \<production JWT secret\> |


Add remote Heroku repository:
```shell
heroku git remote -a mysupportdesk
```
NOTE: `mysupportdesk` is a sample app name, must use the one used for creating heroku application.

Push local changes to Heroku repository:
```shell
git push heroku main
```

Project will be pushed to Heroku, build and deployed.
After that application will be available at Heroku URL.
