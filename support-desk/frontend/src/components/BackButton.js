import { Link } from 'react-router-dom';
import { FaArrowCircleLeft } from 'react-icons/fa';

const BackButton = ({ url }) => (
  <Link to={url} className='btn btn-reverse btn-back'>
    <FaArrowCircleLeft /> Back
  </Link>
);

export {
  BackButton,
};
