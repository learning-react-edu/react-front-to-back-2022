import { Link } from 'react-router-dom';

const TicketItem = ({ ticket }) => (
  <div className='ticket'>
    <div>{new Date(ticket.createdAt).toLocaleString('de-CH')}</div>
    <div>{ticket.product}</div>
    <div className={`status status-${ticket.status}`}>{ticket.status}</div>
    <Link to={`/tickets/${ticket._id}`} className='btn btn-reverse btn-sm'>
      View
    </Link>
  </div>
);

export {
  TicketItem,
};
