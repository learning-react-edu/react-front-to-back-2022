const Spinner = () => (
  <div className='loadingSpinnerContainer'>
    <div className='loadingSpinner'></div>
  </div>
);

export {
  Spinner,
};
