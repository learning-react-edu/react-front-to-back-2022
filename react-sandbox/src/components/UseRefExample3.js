import { useState } from 'react';
import { Todo } from './Todo';

const UseRefExample3 = () => {
  const [ showTodo, setShowTodo ] = useState(true);

  return (
    <>
      {showTodo && <Todo/>}
      <button className="btn btn-primary" onClick={() => setShowTodo(prev => !prev)}>Toggle Todo</button>
    </>
  );
};

export {
  UseRefExample3,
};
