import React from 'react';

const Button = React.memo(({ addTask }) => {
  console.log('Button rendered');

  return (
    <button className='btn btn-primary' onClick={addTask}>Add Task</button>
  );
});

export {
  Button,
};
